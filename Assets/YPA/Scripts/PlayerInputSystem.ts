
namespace game {
  
    @ut.executeAfter(ut.Shared.InputFence)
    export class PlayerInputSystem extends ut.ComponentSystem {

        countReturn = 0;
        openedCards = [];
        isInCTA = false;
        matchCounter = 0;
        ctaEntityNames = ["ButtonCTA", "CTAPanel"];

        OnUpdate(): void {

            this.world.forEach([ut.Entity, ut.Core2D.TransformLocalPosition, ut.Core2D.Sprite2DRenderer, ut.Core2D.Sprite2DSequence, ut.Core2D.Sprite2DSequencePlayer, game.PlayerInput], (entity, position, gfx, seq, player, input) => {   
                if (ut.Runtime.Input.getMouseButton(0) &&
                    PlayerInputSystem.isInBoundries(PlayerInputSystem.getPointerWorldPosition(this.world, this.world.getEntityByName("Camera")), position) && !input.isOpened  ) {
                    {      
                        this.countReturn++;
                        if (this.countReturn <= 2) {
                            input.isOpened = true;
                            player.speed = 2;
                            this.playClip("CardFlip");
                            player.paused = false;
                            this.openCard(entity);
                        }
                    }
                }
            });
            this.world.forEach([ut.Entity, ut.Core2D.TransformLocalPosition, game.CTA], (entity, pos, action) => {
                if (ut.Runtime.Input.getMouseButton(0) &&
                PlayerInputSystem.isCTAButton(PlayerInputSystem.getPointerWorldPosition(this.world, this.world.getEntityByName("Camera")), pos))
                    {
                        CTACaller();
                    }
            });
            this.world.forEach([ut.Entity, ut.Core2D.TransformLocalPosition, ut.Core2D.Sprite2DRendererOptions, game.CTA], (entity, pos, size, action) => {
                if (ut.Runtime.Input.getMouseButton(0) &&
                PlayerInputSystem.isCTASceneButton(PlayerInputSystem.getPointerWorldPosition(this.world, this.world.getEntityByName("Camera")), pos, size) && this.isInCTA)
                    {
                        CTACaller();
                    }
            });

            //});
        }

        /** Returns the point of clicking in world space while considering frame size */
        static getPointerWorldPosition(world: ut.World, cameraEntity: ut.Entity): Vector3 {
            let displayInfo = world.getConfigData(ut.Core2D.DisplayInfo);
            let displaySize = new Vector2(displayInfo.width, displayInfo.height);
            let inputPosition = ut.Runtime.Input.getInputPosition();
            return ut.Core2D.TransformService.windowToWorld(world, cameraEntity, inputPosition, displaySize);
        }

        static isInBoundries(mousePos: Vector3, entityPos: ut.Core2D.TransformLocalPosition) : boolean{
            if (mousePos.x >= (entityPos.position.x - 1.25) && mousePos.x <= (entityPos.position.x + 1.25) && 
                mousePos.y >= (entityPos.position.y - 1.75) && mousePos.y <= (entityPos.position.y + 1.75)){
                return true;
            }
            else{
                return false;
            }
        }

        static isCTAButton(mousePos: Vector3, entityPos: ut.Core2D.TransformLocalPosition): boolean{
            if (mousePos.x >= (entityPos.position.x - 2) && mousePos.x <= (entityPos.position.x + 2) && 
                mousePos.y >= (entityPos.position.y - 0.7) && mousePos.y <= (entityPos.position.y + 0.7)){
                return true;
            }
            else{
                return false;
            }
        }

        static isCTASceneButton(mousePos: Vector3, entityPos: ut.Core2D.TransformLocalPosition, entitySize: ut.Core2D.Sprite2DRendererOptions): boolean{
            if (mousePos.x >= (entityPos.position.x - entitySize.size.x/2) && mousePos.x <= (entityPos.position.x + entitySize.size.x/2) &&
                mousePos.y >= (entityPos.position.y - entitySize.size.y/2) && mousePos.y <= (entityPos.position.y + entitySize.size.y/2)){
                    return true;
                }
            else{
                return false;
            }
        }

        openCard(card: ut.Entity){
            this.openedCards.push(this.world.getEntityName(card));
            if (this.openedCards.length === 2){
                if (this.world.getComponentData(this.world.getEntityByName(this.openedCards[0]), game.CardIdentifier).CardID === this.world.getComponentData(this.world.getEntityByName(this.openedCards[1]), game.CardIdentifier).CardID){
                    setTimeout(() => {
                        this.matched();
                        this.countReturn = 0;
                        this.playClip("CardMatch");    
                        this.matchCounter++;
                        if (this.matchCounter >= 6){
                            setTimeout(() => {
                                this.endGameParticle();    
                            }, 1500);
                            setTimeout(() => {
                                this.changeScene();    
                            }, 4500);
                        }
                    }, 1000);    
                }
                else{
                    this.unMatched();
                    setTimeout(() => {
                        this.playClip("CardMismatch");
                        this.countReturn = 0;
                    }, 900);
                }
            }
        }

        matched(){
            this.playParticle(this.openedCards[1]);
            if (this.world.getComponentData(this.world.getEntityByName(this.openedCards[0]), game.CardIdentifier).CardID == "Sauces"){
                this.world.removeComponent(this.world.getEntityByName("MayoGFX"), ut.Disabled);
                this.world.removeComponent(this.world.getEntityByName("KetchupGFX"), ut.Disabled);
                this.world.usingComponentData(this.world.getEntityByName("MayoGFX"), [game.TranslationAnimation, ut.Core2D.TransformLocalPosition], (anim, pos) =>{
                    anim.Source = this.world.getEntityByName(this.openedCards[1]);
                    anim.StepX = (this.world.getComponentData(anim.Destination, ut.Core2D.TransformLocalPosition).position.x - this.world.getComponentData(anim.Source, ut.Core2D.TransformLocalPosition).position.x) / anim.Steps;
                    anim.StepY = (this.world.getComponentData(anim.Destination, ut.Core2D.TransformLocalPosition).position.y - this.world.getComponentData(anim.Source, ut.Core2D.TransformLocalPosition).position.y) / anim.Steps;
                    pos.position = this.world.getComponentData(anim.Source, ut.Core2D.TransformLocalPosition).position;
                    anim.isActive = true;
                });
                this.world.usingComponentData(this.world.getEntityByName("KetchupGFX"), [game.TranslationAnimation, ut.Core2D.TransformLocalPosition], (anim, pos) =>{
                    anim.Source = this.world.getEntityByName(this.openedCards[1]);
                    anim.StepX = (this.world.getComponentData(anim.Destination, ut.Core2D.TransformLocalPosition).position.x - this.world.getComponentData(anim.Source, ut.Core2D.TransformLocalPosition).position.x) / anim.Steps;
                    anim.StepY = (this.world.getComponentData(anim.Destination, ut.Core2D.TransformLocalPosition).position.y - this.world.getComponentData(anim.Source, ut.Core2D.TransformLocalPosition).position.y) / anim.Steps;
                    pos.position = this.world.getComponentData(anim.Source, ut.Core2D.TransformLocalPosition).position;
                    anim.isActive = true;
                });
            }
            else{
                this.world.removeComponent(this.world.getEntityByName(this.world.getComponentData(this.world.getEntityByName(this.openedCards[0]), game.CardIdentifier).CardID + "GFX"), ut.Disabled);
                this.world.usingComponentData(this.world.getEntityByName(this.world.getComponentData(this.world.getEntityByName(this.openedCards[0]), game.CardIdentifier).CardID + "GFX"), [game.TranslationAnimation, ut.Core2D.TransformLocalPosition], (anim, pos) =>{
                    anim.Source = this.world.getEntityByName(this.openedCards[1]);
                    anim.StepX = (this.world.getComponentData(anim.Destination, ut.Core2D.TransformLocalPosition).position.x - this.world.getComponentData(anim.Source, ut.Core2D.TransformLocalPosition).position.x) / anim.Steps;
                    anim.StepY = (this.world.getComponentData(anim.Destination, ut.Core2D.TransformLocalPosition).position.y - this.world.getComponentData(anim.Source, ut.Core2D.TransformLocalPosition).position.y) / anim.Steps;
                    pos.position = this.world.getComponentData(anim.Source, ut.Core2D.TransformLocalPosition).position;
                    anim.isActive = true;
                });
            }    

            this.world.destroyEntity(this.world.getEntityByName(this.openedCards[0]));
            this.world.destroyEntity(this.world.getEntityByName(this.openedCards[1]));
            this.openedCards = [];
        }

        unMatched(){
            this.world.addComponent(this.world.getEntityByName(this.openedCards[0]), UnMatched);
            this.world.addComponent(this.world.getEntityByName(this.openedCards[1]), UnMatched);
            this.openedCards = [];    
        }

        changeScene(){
            this.world.addComponent(this.world.getEntityByName("Particle"), ut.Disabled);
            this.world.addComponent(this.world.getEntityByName("Particle2"), ut.Disabled);
            this.world.addComponent(this.world.getEntityByName("Particle3"), ut.Disabled);
            this.ctaEntityNames.forEach(entity => {
                this.world.removeComponent(this.world.getEntityByName(entity), ut.Disabled);
            });
            this.world.forEach([ut.Entity, game.MainScene], (entity, main) =>{
                this.world.addComponent(entity, ut.Disabled);
            });
        }

        // Play an AudioClip
        playClip(audioSourceEntityName: string): void {
            let audioSourceEntity = this.world.getEntityByName(audioSourceEntityName);
            if (!this.world.hasComponent(audioSourceEntity, ut.Audio.AudioSourceStart)) {
                this.world.addComponent(audioSourceEntity, ut.Audio.AudioSourceStart);
            }
        }

        // Play particle effect
        playParticle(matchedCard: string): void{
            this.world.usingComponentData(this.world.getEntityByName("Particle"), [ut.Core2D.TransformLocalPosition, ut.Particles.ParticleEmitter, ut.Particles.BurstEmission], (pos, particle, burst) =>{
                this.world.usingComponentData(this.world.getEntityByName("ParticleGFX"), [ut.Core2D.Sprite2DRenderer], (gfx) => {
                    if (this.world.getComponentData(this.world.getEntityByName(matchedCard), game.CardIdentifier).CardID == "Sauces"){
                        gfx.sprite = this.world.getComponentData(this.world.getEntityByName("KetchupGFX"), ut.Core2D.Sprite2DRenderer).sprite;
                        
                    }
                    else{
                        gfx.sprite = this.world.getComponentData(this.world.getEntityByName(this.world.getComponentData(this.world.getEntityByName(matchedCard), game.CardIdentifier).CardID + "GFX"), ut.Core2D.Sprite2DRenderer).sprite;
                    }
                });
                particle.particle = this.world.getEntityByName("ParticleGFX");
                burst.cycles = burst.cycles + 1;
            });
        }

        endGameParticle(){
            this.world.usingComponentData(this.world.getEntityByName("Particle"), [ut.Core2D.TransformLocalPosition, ut.Particles.ParticleEmitter, ut.Particles.BurstEmission], (pos, particle, burst) =>{
                this.world.usingComponentData(this.world.getEntityByName("ParticleGFX"), [ut.Core2D.Sprite2DRenderer], (gfx) => {
                    gfx.sprite = this.world.getComponentData(this.world.getEntityByName("Particle_1"), ut.Core2D.Sprite2DRenderer).sprite;
                });
                particle.particle = this.world.getEntityByName("ParticleGFX");
                this.world.removeComponent(this.world.getEntityByName("Particle"), ut.Particles.BurstEmission);
                particle.maxParticles = 100;
                particle.emitRate = 15;
            });
            this.world.usingComponentData(this.world.getEntityByName("Particle2"), [ut.Core2D.TransformLocalPosition, ut.Particles.ParticleEmitter, ut.Particles.BurstEmission], (pos, particle, burst) =>{
                this.world.usingComponentData(this.world.getEntityByName("ParticleGFX2"), [ut.Core2D.Sprite2DRenderer], (gfx) => {
                    gfx.sprite = this.world.getComponentData(this.world.getEntityByName("Particle_2"), ut.Core2D.Sprite2DRenderer).sprite;
                });
                particle.particle = this.world.getEntityByName("ParticleGFX2");
                this.world.removeComponent(this.world.getEntityByName("Particle2"), ut.Particles.BurstEmission);
                particle.maxParticles = 100;
                particle.emitRate = 15;
            });
            this.world.usingComponentData(this.world.getEntityByName("Particle3"), [ut.Core2D.TransformLocalPosition, ut.Particles.ParticleEmitter, ut.Particles.BurstEmission], (pos, particle, burst) =>{
                this.world.usingComponentData(this.world.getEntityByName("ParticleGFX3"), [ut.Core2D.Sprite2DRenderer], (gfx) => {
                    gfx.sprite = this.world.getComponentData(this.world.getEntityByName("Particle_3"), ut.Core2D.Sprite2DRenderer).sprite;
                });
                particle.particle = this.world.getEntityByName("ParticleGFX3");
                this.world.removeComponent(this.world.getEntityByName("Particle3"), ut.Particles.BurstEmission);
                particle.maxParticles = 100;
                particle.emitRate = 15;
            });
        }
    }
}
