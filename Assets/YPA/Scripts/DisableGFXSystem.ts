
namespace game {

    /** New System */
    @ut.executeBefore(ut.Shared.InputFence)
    export class DisableGFXSystem extends ut.ComponentSystem {
        
        OnUpdate():void {
            this.world.forEach([ut.Entity, game.Activity], (entity, activity) => {
                if (activity.isActive){
                    activity.isActive = false;
                    this.world.addComponent(entity, ut.Disabled);
                }     
            });
        }
    }
}
