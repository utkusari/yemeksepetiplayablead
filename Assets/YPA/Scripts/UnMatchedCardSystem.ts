
namespace game {

    @ut.executeAfter(ut.Shared.InputFence)
    export class UnMatchedCardSystem extends ut.ComponentSystem {
        
        OnUpdate():void {
            let selectedCards = [];
            this.world.forEach([ut.Entity, ut.Core2D.Sprite2DRenderer, game.PlayerInput, game.UnMatched], (entity, gfx, input, unmatch) => {
                selectedCards.push(this.world.getEntityName(entity));
                this.world.removeComponent(entity, game.UnMatched);
            });

            if (selectedCards.length == 2){
                setTimeout(() => {
                    this.world.usingComponentData(this.world.getEntityByName(selectedCards[0]), [ut.Core2D.Sprite2DRenderer, ut.Core2D.Sprite2DSequence, ut.Core2D.Sprite2DSequencePlayer, game.PlayerInput], (gfx, seq, player, input) => {
                        player.time = 1;
                        player.speed = -2;
                        input.isOpened = false;
                    });
                }, 660);
                setTimeout(() => {
                    this.world.usingComponentData(this.world.getEntityByName(selectedCards[1]), [ut.Core2D.Sprite2DRenderer, ut.Core2D.Sprite2DSequence, ut.Core2D.Sprite2DSequencePlayer, game.PlayerInput], (gfx, seq, player, input) => {
                        player.time = 1;
                        player.speed = -2;
                        input.isOpened = false;
                    });

                    setTimeout(() => {
                        this.playClip("CardFlip");
                    }, 200);
                    selectedCards = [];
                    
                }, 1000);
            }
        }

        // Play an AudioClip
        playClip(audioSourceEntityName: string): void {
            let audioSourceEntity = this.world.getEntityByName(audioSourceEntityName);
            if (!this.world.hasComponent(audioSourceEntity, ut.Audio.AudioSourceStart)) {
                this.world.addComponent(audioSourceEntity, ut.Audio.AudioSourceStart);
            }
        }
    }
}
