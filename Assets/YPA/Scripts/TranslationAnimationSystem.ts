
namespace game {

    export class TranslationAnimationSystem extends ut.ComponentSystem {
        OnUpdate():void {
            this.world.forEach([ut.Entity, game.TranslationAnimation, ut.Core2D.TransformLocalPosition], (entity, anim, pos) => {
                if (anim.isActive){
                    if (anim.Count < anim.Steps){
                        this.world.usingComponentData(entity, [game.TranslationAnimation, ut.Core2D.TransformLocalPosition], (anim, pos) =>{
                            pos.position.x = pos.position.x + anim.StepX;
                            pos.position.y = pos.position.y + anim.StepY;
                            anim.Count++;
                        }); 
                    }
                }
            });
        }
    }
}
