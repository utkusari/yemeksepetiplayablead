var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var game;
(function (game) {
    /** New System */
    var DisableGFXSystem = /** @class */ (function (_super) {
        __extends(DisableGFXSystem, _super);
        function DisableGFXSystem() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        DisableGFXSystem.prototype.OnUpdate = function () {
            var _this = this;
            this.world.forEach([ut.Entity, game.Activity], function (entity, activity) {
                if (activity.isActive) {
                    activity.isActive = false;
                    _this.world.addComponent(entity, ut.Disabled);
                }
            });
        };
        DisableGFXSystem = __decorate([
            ut.executeBefore(ut.Shared.InputFence)
        ], DisableGFXSystem);
        return DisableGFXSystem;
    }(ut.ComponentSystem));
    game.DisableGFXSystem = DisableGFXSystem;
})(game || (game = {}));
var game;
(function (game) {
    var PlayerInputSystem = /** @class */ (function (_super) {
        __extends(PlayerInputSystem, _super);
        function PlayerInputSystem() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.countReturn = 0;
            _this.openedCards = [];
            _this.isInCTA = false;
            _this.matchCounter = 0;
            _this.ctaEntityNames = ["ButtonCTA", "CTAPanel"];
            return _this;
        }
        PlayerInputSystem_1 = PlayerInputSystem;
        PlayerInputSystem.prototype.OnUpdate = function () {
            var _this = this;
            this.world.forEach([ut.Entity, ut.Core2D.TransformLocalPosition, ut.Core2D.Sprite2DRenderer, ut.Core2D.Sprite2DSequence, ut.Core2D.Sprite2DSequencePlayer, game.PlayerInput], function (entity, position, gfx, seq, player, input) {
                if (ut.Runtime.Input.getMouseButton(0) &&
                    PlayerInputSystem_1.isInBoundries(PlayerInputSystem_1.getPointerWorldPosition(_this.world, _this.world.getEntityByName("Camera")), position) && !input.isOpened) {
                    {
                        _this.countReturn++;
                        if (_this.countReturn <= 2) {
                            input.isOpened = true;
                            player.speed = 2;
                            _this.playClip("CardFlip");
                            player.paused = false;
                            _this.openCard(entity);
                        }
                    }
                }
            });
            this.world.forEach([ut.Entity, ut.Core2D.TransformLocalPosition, game.CTA], function (entity, pos, action) {
                if (ut.Runtime.Input.getMouseButton(0) &&
                    PlayerInputSystem_1.isCTAButton(PlayerInputSystem_1.getPointerWorldPosition(_this.world, _this.world.getEntityByName("Camera")), pos)) {
                    CTACaller();
                }
            });
            this.world.forEach([ut.Entity, ut.Core2D.TransformLocalPosition, ut.Core2D.Sprite2DRendererOptions, game.CTA], function (entity, pos, size, action) {
                if (ut.Runtime.Input.getMouseButton(0) &&
                    PlayerInputSystem_1.isCTASceneButton(PlayerInputSystem_1.getPointerWorldPosition(_this.world, _this.world.getEntityByName("Camera")), pos, size) && _this.isInCTA) {
                    CTACaller();
                }
            });
            //});
        };
        /** Returns the point of clicking in world space while considering frame size */
        PlayerInputSystem.getPointerWorldPosition = function (world, cameraEntity) {
            var displayInfo = world.getConfigData(ut.Core2D.DisplayInfo);
            var displaySize = new Vector2(displayInfo.width, displayInfo.height);
            var inputPosition = ut.Runtime.Input.getInputPosition();
            return ut.Core2D.TransformService.windowToWorld(world, cameraEntity, inputPosition, displaySize);
        };
        PlayerInputSystem.isInBoundries = function (mousePos, entityPos) {
            if (mousePos.x >= (entityPos.position.x - 1.25) && mousePos.x <= (entityPos.position.x + 1.25) &&
                mousePos.y >= (entityPos.position.y - 1.75) && mousePos.y <= (entityPos.position.y + 1.75)) {
                return true;
            }
            else {
                return false;
            }
        };
        PlayerInputSystem.isCTAButton = function (mousePos, entityPos) {
            if (mousePos.x >= (entityPos.position.x - 2) && mousePos.x <= (entityPos.position.x + 2) &&
                mousePos.y >= (entityPos.position.y - 0.7) && mousePos.y <= (entityPos.position.y + 0.7)) {
                return true;
            }
            else {
                return false;
            }
        };
        PlayerInputSystem.isCTASceneButton = function (mousePos, entityPos, entitySize) {
            if (mousePos.x >= (entityPos.position.x - entitySize.size.x / 2) && mousePos.x <= (entityPos.position.x + entitySize.size.x / 2) &&
                mousePos.y >= (entityPos.position.y - entitySize.size.y / 2) && mousePos.y <= (entityPos.position.y + entitySize.size.y / 2)) {
                return true;
            }
            else {
                return false;
            }
        };
        PlayerInputSystem.prototype.openCard = function (card) {
            var _this = this;
            this.openedCards.push(this.world.getEntityName(card));
            if (this.openedCards.length === 2) {
                if (this.world.getComponentData(this.world.getEntityByName(this.openedCards[0]), game.CardIdentifier).CardID === this.world.getComponentData(this.world.getEntityByName(this.openedCards[1]), game.CardIdentifier).CardID) {
                    setTimeout(function () {
                        _this.matched();
                        _this.countReturn = 0;
                        _this.playClip("CardMatch");
                        _this.matchCounter++;
                        if (_this.matchCounter >= 6) {
                            setTimeout(function () {
                                _this.endGameParticle();
                            }, 1500);
                            setTimeout(function () {
                                _this.changeScene();
                            }, 4500);
                        }
                    }, 1000);
                }
                else {
                    this.unMatched();
                    setTimeout(function () {
                        _this.playClip("CardMismatch");
                        _this.countReturn = 0;
                    }, 900);
                }
            }
        };
        PlayerInputSystem.prototype.matched = function () {
            var _this = this;
            this.playParticle(this.openedCards[1]);
            if (this.world.getComponentData(this.world.getEntityByName(this.openedCards[0]), game.CardIdentifier).CardID == "Sauces") {
                this.world.removeComponent(this.world.getEntityByName("MayoGFX"), ut.Disabled);
                this.world.removeComponent(this.world.getEntityByName("KetchupGFX"), ut.Disabled);
                this.world.usingComponentData(this.world.getEntityByName("MayoGFX"), [game.TranslationAnimation, ut.Core2D.TransformLocalPosition], function (anim, pos) {
                    anim.Source = _this.world.getEntityByName(_this.openedCards[1]);
                    anim.StepX = (_this.world.getComponentData(anim.Destination, ut.Core2D.TransformLocalPosition).position.x - _this.world.getComponentData(anim.Source, ut.Core2D.TransformLocalPosition).position.x) / anim.Steps;
                    anim.StepY = (_this.world.getComponentData(anim.Destination, ut.Core2D.TransformLocalPosition).position.y - _this.world.getComponentData(anim.Source, ut.Core2D.TransformLocalPosition).position.y) / anim.Steps;
                    pos.position = _this.world.getComponentData(anim.Source, ut.Core2D.TransformLocalPosition).position;
                    anim.isActive = true;
                });
                this.world.usingComponentData(this.world.getEntityByName("KetchupGFX"), [game.TranslationAnimation, ut.Core2D.TransformLocalPosition], function (anim, pos) {
                    anim.Source = _this.world.getEntityByName(_this.openedCards[1]);
                    anim.StepX = (_this.world.getComponentData(anim.Destination, ut.Core2D.TransformLocalPosition).position.x - _this.world.getComponentData(anim.Source, ut.Core2D.TransformLocalPosition).position.x) / anim.Steps;
                    anim.StepY = (_this.world.getComponentData(anim.Destination, ut.Core2D.TransformLocalPosition).position.y - _this.world.getComponentData(anim.Source, ut.Core2D.TransformLocalPosition).position.y) / anim.Steps;
                    pos.position = _this.world.getComponentData(anim.Source, ut.Core2D.TransformLocalPosition).position;
                    anim.isActive = true;
                });
            }
            else {
                this.world.removeComponent(this.world.getEntityByName(this.world.getComponentData(this.world.getEntityByName(this.openedCards[0]), game.CardIdentifier).CardID + "GFX"), ut.Disabled);
                this.world.usingComponentData(this.world.getEntityByName(this.world.getComponentData(this.world.getEntityByName(this.openedCards[0]), game.CardIdentifier).CardID + "GFX"), [game.TranslationAnimation, ut.Core2D.TransformLocalPosition], function (anim, pos) {
                    anim.Source = _this.world.getEntityByName(_this.openedCards[1]);
                    anim.StepX = (_this.world.getComponentData(anim.Destination, ut.Core2D.TransformLocalPosition).position.x - _this.world.getComponentData(anim.Source, ut.Core2D.TransformLocalPosition).position.x) / anim.Steps;
                    anim.StepY = (_this.world.getComponentData(anim.Destination, ut.Core2D.TransformLocalPosition).position.y - _this.world.getComponentData(anim.Source, ut.Core2D.TransformLocalPosition).position.y) / anim.Steps;
                    pos.position = _this.world.getComponentData(anim.Source, ut.Core2D.TransformLocalPosition).position;
                    anim.isActive = true;
                });
            }
            this.world.destroyEntity(this.world.getEntityByName(this.openedCards[0]));
            this.world.destroyEntity(this.world.getEntityByName(this.openedCards[1]));
            this.openedCards = [];
        };
        PlayerInputSystem.prototype.unMatched = function () {
            this.world.addComponent(this.world.getEntityByName(this.openedCards[0]), game.UnMatched);
            this.world.addComponent(this.world.getEntityByName(this.openedCards[1]), game.UnMatched);
            this.openedCards = [];
        };
        PlayerInputSystem.prototype.changeScene = function () {
            var _this = this;
            this.world.addComponent(this.world.getEntityByName("Particle"), ut.Disabled);
            this.world.addComponent(this.world.getEntityByName("Particle2"), ut.Disabled);
            this.world.addComponent(this.world.getEntityByName("Particle3"), ut.Disabled);
            this.ctaEntityNames.forEach(function (entity) {
                _this.world.removeComponent(_this.world.getEntityByName(entity), ut.Disabled);
            });
            this.world.forEach([ut.Entity, game.MainScene], function (entity, main) {
                _this.world.addComponent(entity, ut.Disabled);
            });
        };
        // Play an AudioClip
        PlayerInputSystem.prototype.playClip = function (audioSourceEntityName) {
            var audioSourceEntity = this.world.getEntityByName(audioSourceEntityName);
            if (!this.world.hasComponent(audioSourceEntity, ut.Audio.AudioSourceStart)) {
                this.world.addComponent(audioSourceEntity, ut.Audio.AudioSourceStart);
            }
        };
        // Play particle effect
        PlayerInputSystem.prototype.playParticle = function (matchedCard) {
            var _this = this;
            this.world.usingComponentData(this.world.getEntityByName("Particle"), [ut.Core2D.TransformLocalPosition, ut.Particles.ParticleEmitter, ut.Particles.BurstEmission], function (pos, particle, burst) {
                _this.world.usingComponentData(_this.world.getEntityByName("ParticleGFX"), [ut.Core2D.Sprite2DRenderer], function (gfx) {
                    if (_this.world.getComponentData(_this.world.getEntityByName(matchedCard), game.CardIdentifier).CardID == "Sauces") {
                        gfx.sprite = _this.world.getComponentData(_this.world.getEntityByName("KetchupGFX"), ut.Core2D.Sprite2DRenderer).sprite;
                    }
                    else {
                        gfx.sprite = _this.world.getComponentData(_this.world.getEntityByName(_this.world.getComponentData(_this.world.getEntityByName(matchedCard), game.CardIdentifier).CardID + "GFX"), ut.Core2D.Sprite2DRenderer).sprite;
                    }
                });
                particle.particle = _this.world.getEntityByName("ParticleGFX");
                burst.cycles = burst.cycles + 1;
            });
        };
        PlayerInputSystem.prototype.endGameParticle = function () {
            var _this = this;
            this.world.usingComponentData(this.world.getEntityByName("Particle"), [ut.Core2D.TransformLocalPosition, ut.Particles.ParticleEmitter, ut.Particles.BurstEmission], function (pos, particle, burst) {
                _this.world.usingComponentData(_this.world.getEntityByName("ParticleGFX"), [ut.Core2D.Sprite2DRenderer], function (gfx) {
                    gfx.sprite = _this.world.getComponentData(_this.world.getEntityByName("Particle_1"), ut.Core2D.Sprite2DRenderer).sprite;
                });
                particle.particle = _this.world.getEntityByName("ParticleGFX");
                _this.world.removeComponent(_this.world.getEntityByName("Particle"), ut.Particles.BurstEmission);
                particle.maxParticles = 100;
                particle.emitRate = 15;
            });
            this.world.usingComponentData(this.world.getEntityByName("Particle2"), [ut.Core2D.TransformLocalPosition, ut.Particles.ParticleEmitter, ut.Particles.BurstEmission], function (pos, particle, burst) {
                _this.world.usingComponentData(_this.world.getEntityByName("ParticleGFX2"), [ut.Core2D.Sprite2DRenderer], function (gfx) {
                    gfx.sprite = _this.world.getComponentData(_this.world.getEntityByName("Particle_2"), ut.Core2D.Sprite2DRenderer).sprite;
                });
                particle.particle = _this.world.getEntityByName("ParticleGFX2");
                _this.world.removeComponent(_this.world.getEntityByName("Particle2"), ut.Particles.BurstEmission);
                particle.maxParticles = 100;
                particle.emitRate = 15;
            });
            this.world.usingComponentData(this.world.getEntityByName("Particle3"), [ut.Core2D.TransformLocalPosition, ut.Particles.ParticleEmitter, ut.Particles.BurstEmission], function (pos, particle, burst) {
                _this.world.usingComponentData(_this.world.getEntityByName("ParticleGFX3"), [ut.Core2D.Sprite2DRenderer], function (gfx) {
                    gfx.sprite = _this.world.getComponentData(_this.world.getEntityByName("Particle_3"), ut.Core2D.Sprite2DRenderer).sprite;
                });
                particle.particle = _this.world.getEntityByName("ParticleGFX3");
                _this.world.removeComponent(_this.world.getEntityByName("Particle3"), ut.Particles.BurstEmission);
                particle.maxParticles = 100;
                particle.emitRate = 15;
            });
        };
        var PlayerInputSystem_1;
        PlayerInputSystem = PlayerInputSystem_1 = __decorate([
            ut.executeAfter(ut.Shared.InputFence)
        ], PlayerInputSystem);
        return PlayerInputSystem;
    }(ut.ComponentSystem));
    game.PlayerInputSystem = PlayerInputSystem;
})(game || (game = {}));
var game;
(function (game) {
    var TranslationAnimationSystem = /** @class */ (function (_super) {
        __extends(TranslationAnimationSystem, _super);
        function TranslationAnimationSystem() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        TranslationAnimationSystem.prototype.OnUpdate = function () {
            var _this = this;
            this.world.forEach([ut.Entity, game.TranslationAnimation, ut.Core2D.TransformLocalPosition], function (entity, anim, pos) {
                if (anim.isActive) {
                    if (anim.Count < anim.Steps) {
                        _this.world.usingComponentData(entity, [game.TranslationAnimation, ut.Core2D.TransformLocalPosition], function (anim, pos) {
                            pos.position.x = pos.position.x + anim.StepX;
                            pos.position.y = pos.position.y + anim.StepY;
                            anim.Count++;
                        });
                    }
                }
            });
        };
        return TranslationAnimationSystem;
    }(ut.ComponentSystem));
    game.TranslationAnimationSystem = TranslationAnimationSystem;
})(game || (game = {}));
var game;
(function (game) {
    var UnMatchedCardSystem = /** @class */ (function (_super) {
        __extends(UnMatchedCardSystem, _super);
        function UnMatchedCardSystem() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        UnMatchedCardSystem.prototype.OnUpdate = function () {
            var _this = this;
            var selectedCards = [];
            this.world.forEach([ut.Entity, ut.Core2D.Sprite2DRenderer, game.PlayerInput, game.UnMatched], function (entity, gfx, input, unmatch) {
                selectedCards.push(_this.world.getEntityName(entity));
                _this.world.removeComponent(entity, game.UnMatched);
            });
            if (selectedCards.length == 2) {
                setTimeout(function () {
                    _this.world.usingComponentData(_this.world.getEntityByName(selectedCards[0]), [ut.Core2D.Sprite2DRenderer, ut.Core2D.Sprite2DSequence, ut.Core2D.Sprite2DSequencePlayer, game.PlayerInput], function (gfx, seq, player, input) {
                        player.time = 1;
                        player.speed = -2;
                        input.isOpened = false;
                    });
                }, 660);
                setTimeout(function () {
                    _this.world.usingComponentData(_this.world.getEntityByName(selectedCards[1]), [ut.Core2D.Sprite2DRenderer, ut.Core2D.Sprite2DSequence, ut.Core2D.Sprite2DSequencePlayer, game.PlayerInput], function (gfx, seq, player, input) {
                        player.time = 1;
                        player.speed = -2;
                        input.isOpened = false;
                    });
                    setTimeout(function () {
                        _this.playClip("CardFlip");
                    }, 200);
                    selectedCards = [];
                }, 1000);
            }
        };
        // Play an AudioClip
        UnMatchedCardSystem.prototype.playClip = function (audioSourceEntityName) {
            var audioSourceEntity = this.world.getEntityByName(audioSourceEntityName);
            if (!this.world.hasComponent(audioSourceEntity, ut.Audio.AudioSourceStart)) {
                this.world.addComponent(audioSourceEntity, ut.Audio.AudioSourceStart);
            }
        };
        UnMatchedCardSystem = __decorate([
            ut.executeAfter(ut.Shared.InputFence)
        ], UnMatchedCardSystem);
        return UnMatchedCardSystem;
    }(ut.ComponentSystem));
    game.UnMatchedCardSystem = UnMatchedCardSystem;
})(game || (game = {}));
var ut;
(function (ut) {
    var EntityGroup = /** @class */ (function () {
        function EntityGroup() {
        }
        /**
         * @method
         * @desc Creates a new instance of the given entity group by name and returns all entities
         * @param {ut.World} world - The world to add to
         * @param {string} name - The fully qualified name of the entity group
         * @returns Flat list of all created entities
         */
        EntityGroup.instantiate = function (world, name) {
            var data = this.getEntityGroupData(name);
            if (data == undefined)
                throw "ut.EntityGroup.instantiate: No 'EntityGroup' was found with the name '" + name + "'";
            return data.load(world);
        };
        ;
        /**
         * @method
         * @desc Destroys all entities that were instantated with the given group name
         * @param {ut.World} world - The world to destroy from
         * @param {string} name - The fully qualified name of the entity group
         */
        EntityGroup.destroyAll = function (world, name) {
            var type = this.getEntityGroupData(name).Component;
            world.forEach([ut.Entity, type], function (entity, instance) {
                // @TODO This should REALLY not be necessary
                // We are protecting against duplicate calls to `destroyAllEntityGroups` within an iteration
                if (world.exists(entity)) {
                    world.destroyEntity(entity);
                }
            });
        };
        /**
         * @method
         * @desc Returns an entity group object by name
         * @param {string} name - Fully qualified group name
         */
        EntityGroup.getEntityGroupData = function (name) {
            var parts = name.split('.');
            if (parts.length < 2)
                throw "ut.Streaming.StreamingService.getEntityGroupData: name entry is invalid";
            var shiftedParts = parts.shift();
            var initialData = entities[shiftedParts];
            if (initialData == undefined)
                throw "ut.Streaming.StreamingService.getEntityGroupData: name entry is invalid";
            return parts.reduce(function (v, p) {
                return v[p];
            }, initialData);
        };
        return EntityGroup;
    }());
    ut.EntityGroup = EntityGroup;
})(ut || (ut = {}));
var ut;
(function (ut) {
    var EntityLookupCache = /** @class */ (function () {
        function EntityLookupCache() {
        }
        EntityLookupCache.getByName = function (world, name) {
            var entity;
            if (name in this._cache) {
                entity = this._cache[name];
                if (world.exists(entity))
                    return entity;
            }
            entity = world.getEntityByName(name);
            this._cache[name] = entity;
            return entity;
        };
        EntityLookupCache._cache = {};
        return EntityLookupCache;
    }());
    ut.EntityLookupCache = EntityLookupCache;
})(ut || (ut = {}));
function CTACaller() { FbPlayableAd.onCTAClick(); }
//# sourceMappingURL=tsc-emit.js.map