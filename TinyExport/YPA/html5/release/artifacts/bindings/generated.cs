﻿using UTiny;
using UTiny.Core2D;
using UTiny.Math;
using UTiny.Shared;
using ut;
using UTiny.HTML;
using UTiny.Rendering;
using ut.EditorExtensions;
using UTiny.HitBox2D;
using UTiny.UIControls;
using UTiny.Audio;
using UTiny.Particles;
using UTiny.Interpolation;

/*
 * !!! TEMP UNITL PROPER SCENE FORMAT !!!
 */
namespace entities.game
{
    namespace CTA
    {
        public struct Component : IComponentData
        {
        }
    }
    namespace MainScene
    {
        public struct Component : IComponentData
        {
        }
    }
}

namespace game
{
    public struct Activity : IComponentData
    {
        public bool isActive;
    }
    public struct CardIdentifier : IComponentData
    {
        public string CardID;
    }
    public struct CTA : IComponentData
    {
        public string Link;
    }
    public struct CTAScene : IComponentData
    {
    }
    public struct MainScene : IComponentData
    {
    }
    public struct PlayerInput : IComponentData
    {
        public Entity CardOpened;
        public Entity CardClosed;
        public bool isOpened;
    }
    public struct TranslationAnimation : IComponentData
    {
        public Entity Source;
        public Entity Destination;
        public bool isActive;
        public float StepX;
        public float StepY;
        public int Count;
        public int Steps;
    }
    public struct UnMatched : IComponentData
    {
        public bool unMatched;
    }
}

namespace ut.Core2D
{
    namespace layers
    {
        public struct Default : IComponentData
        {
        }
        public struct TransparentFX : IComponentData
        {
        }
        public struct IgnoreRaycast : IComponentData
        {
        }
        public struct Water : IComponentData
        {
        }
        public struct UI : IComponentData
        {
        }
    }
}

namespace ut.Math
{
}

namespace ut
{
}

namespace ut.Shared
{
}

namespace ut.Core2D
{
}

namespace ut
{
}

namespace ut.HTML
{
}

namespace ut.Rendering
{
}

namespace ut.Rendering
{
}

namespace ut.HTML
{
}

namespace ut.Core2D
{
}

namespace ut.Rendering
{
}

namespace ut.Rendering
{
}

namespace ut.Core2D
{
}

namespace ut.EditorExtensions
{
    public struct AssetReferenceAnimationClip : IComponentData
    {
        public string guid;
        public long fileId;
        public int type;
    }
    public struct AssetReferenceAudioClip : IComponentData
    {
        public string guid;
        public long fileId;
        public int type;
    }
    public struct AssetReferenceSprite : IComponentData
    {
        public string guid;
        public long fileId;
        public int type;
    }
    public struct AssetReferenceSpriteAtlas : IComponentData
    {
        public string guid;
        public long fileId;
        public int type;
    }
    public struct AssetReferenceTexture2D : IComponentData
    {
        public string guid;
        public long fileId;
        public int type;
    }
    public struct AssetReferenceTileBase : IComponentData
    {
        public string guid;
        public long fileId;
        public int type;
    }
    public struct AssetReferenceTMP_FontAsset : IComponentData
    {
        public string guid;
        public long fileId;
        public int type;
    }
    public struct CameraCullingMask : IComponentData
    {
        public int mask;
    }
    public struct EntityLayer : IComponentData
    {
        public int layer;
    }
}

namespace ut.HitBox2D
{
}

namespace ut.UIControls
{
}

namespace ut.Audio
{
}

namespace ut.Particles
{
}

namespace ut.Interpolation
{
}
namespace game
{
    [UpdateBefore(typeof(UTiny.Shared.InputFence))]
    public class DisableGFXSystemJS : IComponentSystem
    {
    }
}
namespace game
{
    [UpdateAfter(typeof(UTiny.Shared.InputFence))]
    public class PlayerInputSystemJS : IComponentSystem
    {
    }
}
namespace game
{
    public class TranslationAnimationSystemJS : IComponentSystem
    {
    }
}
namespace game
{
    [UpdateAfter(typeof(UTiny.Shared.InputFence))]
    public class UnMatchedCardSystemJS : IComponentSystem
    {
    }
}
