﻿/**
 * TINY GENERATED CODE, DO NOT EDIT BY HAND
 * @project YPA
 */

console.log('runtime version: internal');

ut.importModule(ut.Core2D);
ut.importModule(ut.Math);
ut.importModule(ut);
ut.importModule(ut.Shared);
ut.importModule(ut.Core2D);
ut.importModule(ut.HTML);
ut.importModule(ut.Rendering);
ut.importModule(ut.Rendering);
ut.importModule(ut.HTML);
ut.importModule(ut.Core2D);
ut.importModule(ut.Rendering);
ut.importModule(ut.Rendering);
ut.importModule(ut.Core2D);
ut.importModule(ut.Physics2D);
ut.importModule(ut.HitBox2D);
ut.importModule(ut.Tilemap2D);
ut.importModule(ut.UIControls);
ut.importModule(ut.UILayout);
ut.importModule(ut.Text);
ut.importModule(ut.HTML);
ut.importModule(ut.Audio);
ut.importModule(ut.Video);
ut.importModule(ut.PlayableAd);
ut.main = function() {
    // Singleton world
    var world = new ut.World();

    // Schedule all systems
    var scheduler = world.scheduler();
    game.DisableGFXSystemJS.update = new game.DisableGFXSystem()._MakeSystemFn();
    game.PlayerInputSystemJS.update = new game.PlayerInputSystem()._MakeSystemFn();
    game.UnMatchedCardSystemJS.update = new game.UnMatchedCardSystem()._MakeSystemFn();
    scheduler.schedule(game.DisableGFXSystemJS);
    scheduler.schedule(ut.HTML.InputHandler);
    scheduler.schedule(ut.HTML.AssetLoader);
    scheduler.schedule(ut.Core2D.SequencePlayerSystem);
    scheduler.schedule(ut.HitBox2D.HitBox2DSystem);
    scheduler.schedule(ut.Shared.InputFence);
    scheduler.schedule(game.PlayerInputSystemJS);
    scheduler.schedule(game.UnMatchedCardSystemJS);
    scheduler.schedule(ut.UIControls.MouseInteractionSystem);
    scheduler.schedule(ut.UIControls.ToggleCheckedSystem);
    scheduler.schedule(ut.Shared.UserCodeStart);
    scheduler.schedule(ut.Shared.UserCodeEnd);
    scheduler.schedule(ut.Tilemap2D.TilemapChunkingSystem);
    scheduler.schedule(ut.UIControls.UIControlsSystem);
    scheduler.schedule(ut.UIControls.ButtonSystem);
    scheduler.schedule(ut.UIControls.ToggleSystem);
    scheduler.schedule(ut.UILayout.UICanvasSystem);
    scheduler.schedule(ut.UILayout.UILayoutSystem);
    scheduler.schedule(ut.UILayout.SetSprite2DSizeSystem);
    scheduler.schedule(ut.UILayout.SetRectTransformSizeSystem);
    scheduler.schedule(ut.HTML.TextMeasurement);
    scheduler.schedule(ut.Shared.RenderingFence);
    scheduler.schedule(ut.Core2D.UpdateLocalTransformSystem);
    scheduler.schedule(ut.Core2D.UpdateWorldTransformSystem);
    scheduler.schedule(ut.Core2D.Sprite2DInitSystem);
    scheduler.schedule(ut.Text.Text2DInitSystem);
    scheduler.schedule(ut.Core2D.DisplayList);
    scheduler.schedule(ut.Shared.PlatformRenderingFence);
    scheduler.schedule(ut.Rendering.RendererCanvas);
    scheduler.schedule(ut.Rendering.RendererGLWebGL);
    scheduler.schedule(ut.Physics2D.Physics2DSystem);
    scheduler.schedule(ut.Audio.AudioSystem);
    scheduler.schedule(ut.Video.VideoSystem);

    // Initialize all configuration data
    var c0 = world.getConfigData(ut.Core2D.DisplayInfo);
    c0.width = 1920;
    c0.height = 1080;
    c0.autoSizeToFrame = true;
    c0.renderMode = 0;
    world.setConfigData(c0);
    var c1 = world.getConfigData(ut.Physics2D.Physics2DConfig);
    var s0 = new ut.Math.Vector2();
    s0.x = 0;
    s0.y = -10;
    c1.gravity = s0;
    world.setConfigData(c1);
    var c2 = world.getConfigData(ut.Audio.AudioConfig);
    world.setConfigData(c2);
    var c3 = world.getConfigData(ut.PlayableAd.PlayableAdInfo);
    world.setConfigData(c3);

    // Create and initialize all resource entities
    UT_ASSETS_SETUP(world);

    // Create and initialize all startup entities
    ut.EntityGroup.instantiate(world, "game.MainScene");

    // Set up the WebSocket client
    ut._wsclient = ut._wsclient || {};
    ut._wsclient.world = world;

    // Start the player loop
    try { ut.Runtime.Service.run(world); } catch (e) { if (e !== 'SimulateInfiniteLoop') throw e; }
}
